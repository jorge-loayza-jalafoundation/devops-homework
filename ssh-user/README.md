
# User creation and SSH connection

## User creation

To create a user, use the `useradd` command as follows with administrator privileges:

```sh
# useradd -m -s <user-shell> <user-name>
```

* The option `-m` creates a home diretory for the user.
* The `<user-shell>` can be any of those listed in the file `/etc/shells`. For example, to create a user with the name of `user-test` and with Z shell as login shell the command is as follow:

  ```sh
  # useradd -m -s /bin/zsh user-test
  ```

After creating the user, it is necessary to establish a password for it, this is achieved with the `passwd` command. 

To enter the user's password, the following command must be entered with administrator privileges:

```sh
# passwd <user-name>
```

Then you have to enter the password for the user.
> To delete the user's password or set it to blank, execute the command with the `-d` option.

Also, to add the user to a specific group just enter the command `usermod` as follow:

```sh
# usermod -aG <group> <user-name>
```

> To provide administrator privileges you have to add the user to the `sudo` group in Ubuntu or to the `wheel` group in fedora.

Finally, to log in as the new user, enter the following command:

```sh
$ su - <user-name>
```
## SSH connection

Requirements:
* The user host must have installed the ssh client. The package to install may vary depending on the OS, in Ubuntu the package to install is `openssh-client`. 
* The host to which we connect must have the ssh server installed. The package to install may vary depending on the OS, in Ubuntu the package to install is `openssh-server`. 

To connect to a host using SSH we must first generate the private and public keys for our user with the following command:

```sh
$ ssh-keygen
```
After enterig the requested information the public and private rsa keys are generated. The default directory where the keys are stored is `~./ssh`.

Now we have to copy our public key to the host we want to connect, to do that we have to entere the following command:

```sh
$ ssh-copy-id <user-name>@<host-ip>
```

The above command uses the default location in `~./ssh` for public key, if we need to use a specific public key is necessary to add the option: `-i` as follow.

```sh
$ ssh-copy-id -i <public-key-file-path> <user-name>@<host-ip>
```

And finally, to connect to the host by ssh we just need to enter the following command:

```sh
$ ssh <user-name>@<host-ip>
```

