# VBOX-CLI-TOOL

Simple Virtualbox cli tool to manage virtual machines.

## Requirements
The following dependencies must be installed and well configured:

1. [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
1. [Java JDK 11](https://openjdk.java.net/install/)
2. [Gradle](https://gradle.org/install/)
## Instructions

Open a terminal in the project folder and enter the following commands.

### Build the project

The following command builds the project.

```bash
$ gradle build
```
Or

```bash
$ ./gradlew build
```

The result jar file is located in `app/build/libs` folder.

### Usage

To use the tool just go to the folder `app/build/libs` and enter the following comand:

```bash
$ java -Dvbox.home=/usr/lib/virtualbox -jar app.jar -l
```
> You must set the `-Dvox.home` variable according to where the virtualbox libraries are installed. In Ubuntu the path is **"/usr/lib/virtualbox"** and in fedora it is **"/usr/lib64/virtualbox".**

### Commands

- `-l` List all the virtual machines.
- `-c` Create a machine.
- `-s` Start/Lauch a machine.

