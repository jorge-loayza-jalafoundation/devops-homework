# Jenkins using docker

## Requirements

To run jenkins as a docker container the following programs need to be installed and configured:
1. [Docker](https://docs.docker.com/engine/install/)
2. [Docker Compose](https://docs.docker.com/compose/install/)

## Usage

To run the container just enter the following comand (For unix based system use `sudo` if the user **isn't** in the docker group):

```sh
$ docker-compose up -d
```

The first time the container is executed, it asks us for an administrator password, which is obtained by executing the following command:

```
$ docker exec jenkins-server cat /var/jenkins_home/secrets/initialAdminPassword
```