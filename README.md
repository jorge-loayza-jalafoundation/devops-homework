# DevOps Homework 
This repository includes the following projects:

* [CLI to manage Virtualbox virtual machines.](https://gitlab.com/jorge.loayza/devops-homework/-/tree/main/vbox-cli-tool) 

* [Steps to create a user and make a ssh conection](https://gitlab.com/jorge.loayza/devops-homework/-/tree/main/ssh-user) 

* [Jenkins implementation using docker container](https://gitlab.com/jorge.loayza/devops-homework/-/tree/main/jenkins-docker)